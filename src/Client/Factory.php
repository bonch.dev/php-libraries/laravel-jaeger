<?php


namespace BonchDev\LaravelJaeger\Client;

use Chocofamilyme\LaravelJaeger\Jaeger;
use Illuminate\Http\Client\Factory as BaseFactory;
use Illuminate\Http\Client\PendingRequest;

class Factory extends BaseFactory
{
    public function __call($method, $parameters)
    {
        if (static::hasMacro($method)) {
            return $this->macroCall($method, $parameters);
        }

        return tap(new PendingRequest($this), function ($request) {
            $jaeger = \App::make(Jaeger::class);

            $headers = [];

            $jaeger->inject($headers);

            $request->withHeaders(
                $headers
            )
                ->stub($this->stubCallbacks);
        })->{$method}(...$parameters);
    }
}