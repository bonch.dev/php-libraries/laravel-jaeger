<?php


namespace BonchDev\LaravelJaeger\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class RegisterLaravelJaegerServiceProvider extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laravel-jaeger:provider';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Register LaravelJaegerServiceProvider';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->comment('Publishing LaravelJaeger Service Provider...');
        $this->callSilent('vendor:publish', ['--tag' => 'laravel-jaeger-provider']);

        $namespace = Str::replaceLast('\\', '', $this->laravel->getNamespace());

        $appConfig = file_get_contents(config_path('app.php'));

        if (Str::contains($appConfig, $namespace.'\\Providers\\LaravelJaegerServiceProvider::class')) {
            return;
        }

        $lineEndingCount = [
            "\r\n" => substr_count($appConfig, "\r\n"),
            "\r" => substr_count($appConfig, "\r"),
            "\n" => substr_count($appConfig, "\n"),
        ];

        $eol = array_keys($lineEndingCount, max($lineEndingCount))[0];

        file_put_contents(config_path('app.php'), str_replace(
            "{$namespace}\\Providers\RouteServiceProvider::class,".$eol,
            "{$namespace}\\Providers\RouteServiceProvider::class,".$eol."        {$namespace}\Providers\LaravelJaegerServiceProvider::class,".$eol,
            $appConfig
        ));

        file_put_contents(app_path('Providers/LaravelJaegerServiceProvider.php'), str_replace(
            "namespace App\Providers;",
            "namespace {$namespace}\Providers;",
            file_get_contents(app_path('Providers/LaravelJaegerServiceProvider.php'))
        ));
    }
}