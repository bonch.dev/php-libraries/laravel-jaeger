<?php

namespace BonchDev\LaravelJaeger;

use BonchDev\LaravelJaeger\Client\Factory;
use BonchDev\LaravelJaeger\Commands\RegisterLaravelJaegerServiceProvider;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\Client\Factory as BaseFactory;

class ServiceProvider extends BaseServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../stubs/LaravelJaegerServiceProvider.stub' => app_path('Providers/LaravelJaegerServiceProvider.php'),
        ], 'laravel-jaeger-provider');

        $this->publishes([
            __DIR__.'/../config/laravel-jaeger.php' => config_path('laravel-jaeger.php'),
        ], 'laravel-jaeger-config');
    }

    public function register()
    {
        $this->commands([
            RegisterLaravelJaegerServiceProvider::class
        ]);

        $this->app->bind(
            BaseFactory::class,
            function ($app) {
                return new Factory($app->make(Dispatcher::class));
            }
        );
    }
}