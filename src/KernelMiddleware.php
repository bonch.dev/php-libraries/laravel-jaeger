<?php


namespace BonchDev\LaravelJaeger;


use App;
use Illuminate\Support\Str;

/**
 * Trait KernelMiddleware
 * @package BonchDev\LaravelJaeger
 */
trait KernelMiddleware
{
    public function getMiddlewareList()
    {
        $globalMiddlewares = collect($this->middleware);
        $routeMiddlewares = collect($this->routeMiddleware);
        $groupMiddlewares = collect();

        foreach ($this->middlewareGroups as $group => $middlewares) {
            foreach ($middlewares as $middleware) {
                $middlewareKey = explode(":", $middleware)[0];

                if (isset($routeMiddlewares[$middlewareKey])) {
                    $middleware = $routeMiddlewares[$middlewareKey];
                }

                $groupMiddlewares = $groupMiddlewares->push($middleware);
            }
        }

        $routeMiddlewares = $routeMiddlewares->values();

        $middlewareList = collect()->merge(
            $globalMiddlewares
        );

        $middlewareList = $middlewareList->merge(
            $routeMiddlewares
        );

        $middlewareList = $middlewareList->merge(
            $groupMiddlewares
        );

        $middlewareList = $middlewareList->unique();

        $blackList = config("laravel-jaeger.middlewares.blacklist") ?? null;
        $whiteList = config("laravel-jaeger.middlewares.whitelist") ?? null;

        $middlewareList = $middlewareList->filter(function ($value) use ($blackList, $whiteList) {
            if (!$blackList || !$whiteList) {
                return true;
            }

            $blackListNamespaces = collect($blackList["namespaces"]);
            $whiteListNamespaces = collect($whiteList["namespaces"]);

            $inBlackListClasses = in_array($value, $blackList["classes"]);
            $inWhiteListClasses = in_array($value, $whiteList["classes"]);
            $inBlackListNamespaces = $blackListNamespaces->contains(function ($namespace) use ($value) {
                return Str::contains($value, $namespace);
            });
            $inWhiteListNamespaces = $whiteListNamespaces->contains(function ($namespace) use ($value) {
                return Str::contains($value, $namespace);
            });

            return ($inWhiteListNamespaces && !$inBlackListClasses)
                || (!$inBlackListNamespaces && $inWhiteListClasses);
        });

        return $middlewareList;
    }
}