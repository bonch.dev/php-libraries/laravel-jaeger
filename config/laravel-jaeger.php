<?php

return [

    "middlewares" => [
        "whitelist" => [
            "classes" => [],
            "namespaces" => [
                "App\Http\Middleware",
            ],
        ],

        // Disallowed to tracing classes
        "blacklist" => [
            "classes" => [
                // Default middlewares
                "App\Http\Middleware\TrustProxies",
                "App\Http\Middleware\PreventRequestsDuringMaintenance",
                "App\Http\Middleware\TrimStrings",
                "Chocofamilyme\LaravelJaeger\JaegerMiddleware",
                "App\Http\Middleware\Authenticate",
                "App\Http\Middleware\RedirectIfAuthenticated",
                "App\Http\Middleware\EncryptCookies",
                "App\Http\Middleware\VerifyCsrfToken",
            ],
            "namespaces" => [
                // Default Laravel middlewares
                "Illuminate",
            ],
        ]
    ],

    "controllers" => [
        "whitelist" => [
            "classes" => [],
            "namespaces" => [],
        ],
        "blacklist" => [
            "classes" => [],
            "namespaces" => [],
        ],
    ]
];
