# Laravel Jaeger
###### by bonch.dev

### Description
Additional package for `chocofamilyme/laravel-jaeger`, that spaning the controllers,
middlewares and inject tracing header to `PendingReqeust` through custom `BonchDev\LaravelJaeger\Client\Factory` 
instance which extends default `Illuminate\Http\Client\Factory`. If you use `Http` facade, that will contains injected
tracer headers.

### Features

- Inject tracing header to `PendingReqeust` through custom `BonchDev\LaravelJaeger\Client\Factory`
  instance which extends default `Illuminate\Http\Client\Factory`
  
- Spaning middlewares, controllers and controller's actions. Classes can be added in whitelists or blacklists, if you 
  need
  
### Installation

1. `composer require bonch.dev/laravel-jaeger`
2. Add `KernelMiddleware` trait to your `App\Http\Kernel`
3. `php artisan laravel-jaeger:provider`
4. `php artisan vendor:publish` and choose `BonchDev\LaravelJaeger\ServiceProvider`
5. `php artisan vendor:publish` and choose `Chocofamilyme\LaravelJaeger\LaravelJaegerServiceProvider`
6. Customize configs `jaeger.php` and `laravel-jaeger.php` as you wish